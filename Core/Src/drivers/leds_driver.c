/*
 * LedsDriver.c
 *
 *  Created on: 5 de jun de 2021
 *      Author: joseg
 */

#include "gpio.h"

#include "drivers/leds_driver.h"
#include "services/morse_service.h"
#include "utils/timer.h"

#define UNIT_PERIOD ((timer_t)70)

#define DOT_PERIOD UNIT_PERIOD
#define DASH_PERIOD ((timer_t)(UNIT_PERIOD * 3))

#define INTRA_CHARACTER_PERIOD DOT_PERIOD
#define EXTRA_CHARACTER_PERIOD DASH_PERIOD
#define EXTRA_WORD_PERIOD ((timer_t)(UNIT_PERIOD * 7))

#define MAX_ENTRIES_PER_BUFFER 255

struct hal_led {
	GPIO_TypeDef *gpio_port;
	uint16_t gpio_pin;
};

struct morse_entries_buffer {
	const struct morse_entry *entries[MAX_ENTRIES_PER_BUFFER];
	uint8_t char_index;
	uint8_t index;
	uint8_t size;
};

struct led {
	const struct hal_led hal_led;
	struct morse_entries_buffer morse_buffer;

	timer_t blink_timer;
	timer_t blink_period;
	uint8_t is_idle;
	uint8_t is_offset;

	void (*blink_end_callback)();
};

/**
 * @brief Atualiza o led e cuida da exibição do código morse.
 * 
 * @param led Led que será atualizado.
 */
static void led_update(struct led *led);

/**
 * @brief Reinicia a estrutura de um led completamente.
 * 
 * @param led Led que será reiniciado.
 */
static void led_reset(struct led *led);

static struct led leds[LEDS_SIZE] = {
	{ .hal_led = { .gpio_port = RED_LED_GPIO_Port,
		       .gpio_pin = RED_LED_Pin } },
	{ .hal_led = { .gpio_port = GREEN_LED_GPIO_Port,
		       .gpio_pin = GREEN_LED_Pin } },
	{ .hal_led = { .gpio_port = BLUE_LED_GPIO_Port,
		       .gpio_pin = BLUE_LED_Pin } }
};

uint8_t leds_drv_init()
{
	for (uint8_t u = 0; u != LEDS_SIZE; ++u) {
		led_reset(&leds[u]);
	}

	return 0;
}

uint8_t leds_drv_handler()
{
	for (uint8_t u = 0; u != LEDS_SIZE; ++u) {
		struct led *led = &leds[u];
		if (!led->is_idle &&
		    timer_wait_ms(led->blink_timer, led->blink_period)) {
			led_update(led);
		}
	}
	return 0;
}

uint8_t leds_drv_deinit()
{
	for (uint8_t u = 0; u != LEDS_SIZE; ++u) {
		led_reset(&leds[u]);
	}

	return 0;
}

uint8_t leds_drv_blink_morse(enum leds led, uint8_t size,
			     const struct morse_entry **entries,
			     void (*blink_end_callback)())
{
	if (!leds[led].is_idle) {
		return 1;
	}

	led_reset(&leds[led]);
	leds[led].is_idle = 0;
	leds[led].blink_end_callback = blink_end_callback;

	leds[led].morse_buffer.size = size;
	for (uint8_t u = 0; u != size; ++u) {
		leds[led].morse_buffer.entries[u] = entries[u];
	}

	led_update(&leds[led]);

	return 0;
}

static void led_update(struct led *led)
{
	if (led->morse_buffer.index == led->morse_buffer.size) {
		// Esse led transmitiu toda sua mensagem.
		led->is_idle = 1;
		HAL_GPIO_WritePin(led->hal_led.gpio_port, led->hal_led.gpio_pin,
				  GPIO_PIN_RESET);
		if (led->blink_end_callback) {
			led->blink_end_callback();
		}
		return;
	}

	GPIO_PinState updated_state;
	const struct morse_entry *current_entry =
		led->morse_buffer.entries[led->morse_buffer.index];
	if (current_entry) {
		if (!led->is_offset) {
			updated_state = GPIO_PIN_SET;

			led->blink_period =
				(current_entry
					 ->entry[led->morse_buffer.char_index] ==
				 '.') ?
					      DOT_PERIOD :
					      DASH_PERIOD;
			++led->morse_buffer.char_index;
		} else {
			updated_state = GPIO_PIN_RESET;

			if (led->morse_buffer.char_index !=
			    current_entry->entry_size) {
				// Offset dentro do próprio caractere.
				led->blink_period = INTRA_CHARACTER_PERIOD;
			} else {
				// Fim desse caractere.
				led->blink_period = EXTRA_CHARACTER_PERIOD;

				led->morse_buffer.char_index = 0;
				++led->morse_buffer.index;
			}
		}

		led->is_offset = !led->is_offset;
	} else {
		// Entrada nula no buffer significa o espaço entre duas palavras.
		led->blink_period = EXTRA_WORD_PERIOD;
		updated_state = GPIO_PIN_RESET;

		// Avança para próxima entrada da tabela no buffer.
		++led->morse_buffer.index;
	}

	HAL_GPIO_WritePin(led->hal_led.gpio_port, led->hal_led.gpio_pin,
			  updated_state);
	timer_reset(&led->blink_timer);
}

static void led_reset(struct led *led)
{
	HAL_GPIO_WritePin(led->hal_led.gpio_port, led->hal_led.gpio_pin,
			  GPIO_PIN_RESET);

	led->morse_buffer.char_index = 0;
	led->morse_buffer.index = 0;
	led->morse_buffer.size = 0;
	for (uint8_t u = 0; u != MAX_ENTRIES_PER_BUFFER; ++u) {
		led->morse_buffer.entries[u] = NULL;
	}

	led->blink_timer = 0;
	led->blink_period = 0;
	led->is_idle = 1;
	led->is_offset = 0;

	led->blink_end_callback = NULL;
}
