/*
 * morse_service.c
 *
 *  Created on: Jun 5, 2021
 *      Author: joseg
 */
#include <string.h>

#include "services/morse_service.h"

/**
 * @brief Checa se caractere pertence ao alfabeto e é minúsculo.
 * 
 * @param c Caractere que será checado.
 * @return uint8_t 
 */
static uint8_t is_lower_alpha(char c);

/**
 * @brief Checa se caractere pertence ao alfabeto e é maiúsculo.
 * 
 * @param c Caractere que será checado.
 * @return uint8_t 
 */
static uint8_t is_upper_alpha(char c);

/**
 * @brief Checa se caractere é numérico.
 * 
 * @param c Caractere que será checado.
 * @return uint8_t 
 */
static uint8_t is_numeric(char c);

/**
 * @brief Checa se caractere é um símbolo.
 * 
 * @param c Caractere que será checado.
 * @return uint8_t 
 */
static uint8_t is_symbol(char c);

/**
 * @brief Pega um ponteiro de uma entrada na tabela de morse.
 * 
 * @param key Chave que deverá ser buscada na tabela.
 * @return const struct morse_entry* em caso de chave válida, NULL caso contrário.
 */
static const struct morse_entry *get_entry_from_table(char key);

static const struct morse_entry morse_table[] = {
	{ .key = 'a', .entry = ".-", .entry_size = 2 },
	{ .key = 'b', .entry = "-...", .entry_size = 4 },
	{ .key = 'c', .entry = "-.-.", .entry_size = 4 },
	{ .key = 'd', .entry = "-..", .entry_size = 3 },
	{ .key = 'e', .entry = ".", .entry_size = 1 },
	{ .key = 'f', .entry = "..-.", .entry_size = 4 },
	{ .key = 'g', .entry = "--.", .entry_size = 3 },
	{ .key = 'h', .entry = "....", .entry_size = 4 },
	{ .key = 'i', .entry = "..", .entry_size = 2 },
	{ .key = 'j', .entry = ".---", .entry_size = 4 },
	{ .key = 'k', .entry = "-.-", .entry_size = 3 },
	{ .key = 'l', .entry = ".-..", .entry_size = 4 },
	{ .key = 'm', .entry = "--", .entry_size = 2 },
	{ .key = 'n', .entry = "-.", .entry_size = 2 },
	{ .key = 'o', .entry = "---", .entry_size = 3 },
	{ .key = 'p', .entry = ".--.", .entry_size = 4 },
	{ .key = 'q', .entry = "--.-", .entry_size = 4 },
	{ .key = 'r', .entry = ".-.", .entry_size = 3 },
	{ .key = 's', .entry = "...", .entry_size = 3 },
	{ .key = 't', .entry = "-", .entry_size = 1 },
	{ .key = 'u', .entry = "..-", .entry_size = 3 },
	{ .key = 'v', .entry = "...-", .entry_size = 4 },
	{ .key = 'w', .entry = ".--", .entry_size = 3 },
	{ .key = 'x', .entry = "-..-", .entry_size = 4 },
	{ .key = 'y', .entry = "-.--", .entry_size = 4 },
	{ .key = 'z', .entry = "--..", .entry_size = 4 },
	{ .key = '0', .entry = "-----", .entry_size = 5 },
	{ .key = '1', .entry = ".----", .entry_size = 5 },
	{ .key = '2', .entry = "..---", .entry_size = 5 },
	{ .key = '3', .entry = "...--", .entry_size = 5 },
	{ .key = '4', .entry = "....-", .entry_size = 5 },
	{ .key = '5', .entry = ".....", .entry_size = 5 },
	{ .key = '6', .entry = "-....", .entry_size = 5 },
	{ .key = '7', .entry = "--...", .entry_size = 5 },
	{ .key = '8', .entry = "---..", .entry_size = 5 },
	{ .key = '9', .entry = "----.", .entry_size = 5 },
	{ .key = '&', .entry = ".-...", .entry_size = 5 },
	{ .key = '\'', .entry = ".----.", .entry_size = 6 },
	{ .key = '@', .entry = ".--.-.", .entry_size = 6 },
	{ .key = ')', .entry = "-.--.-", .entry_size = 6 },
	{ .key = '(', .entry = "-.--.", .entry_size = 5 },
	{ .key = ':', .entry = "---...", .entry_size = 6 },
	{ .key = ',', .entry = "--..--", .entry_size = 6 },
	{ .key = '=', .entry = "-...-", .entry_size = 5 },
	{ .key = '.', .entry = ".-.-.-", .entry_size = 6 },
	{ .key = '-', .entry = "-....-", .entry_size = 6 },
	{ .key = '%', .entry = "------..-.-----", .entry_size = 15 },
	{ .key = '+', .entry = ".-.-.", .entry_size = 5 },
	{ .key = '\"', .entry = ".-..-.", .entry_size = 6 },
	{ .key = '?', .entry = "..--..", .entry_size = 6 },
	{ .key = '/', .entry = "-..-.", .entry_size = 5 },
	{ .key = '!', .entry = "-.-.--", .entry_size = 6 },
	{ .key = ' ', .entry = NULL, .entry_size = 0 }, // Last entry
};

uint8_t morse_srv_init()
{
	// Checa integridade da tabela.
	// Só vê se o tamanho gravado bate com o tamanho real da entrada.
	for (uint8_t u = 0; morse_table[u].entry != NULL; ++u) {
		if (morse_table[u].entry_size != strlen(morse_table[u].entry)) {
			return 1;
		}
	}

	return leds_drv_init();
}

uint8_t morse_srv_handler()
{
	return leds_drv_handler();
}

uint8_t morse_srv_deinit()
{
	return leds_drv_deinit();
}

uint8_t morse_srv_blink(enum leds led, const char *message,
			void (*end_callback)())
{
	if (led < 0 || led >= LEDS_SIZE || !message) {
		return 1;
	}

	const uint8_t message_size = strlen(message);

	const struct morse_entry *entries[message_size];
	uint8_t entries_found = 0;

	for (uint8_t u = 0; u != message_size; ++u) {
		char c = message[u];

		if (!is_lower_alpha(c) && !is_upper_alpha(c) &&
		    !is_numeric(c) && !is_symbol(c) && c != ' ') {
			// Caractere inválido -> mensagem inválida.
			return 1;
		}

		// Transforma letras maiúsculas em minúsculas pois as chaves na tabela
		// estão dessa forma.
		if (is_upper_alpha(c)) {
			c += 32;
		}

		const struct morse_entry *entry;
		if (c != ' ') {
			if ((entry = get_entry_from_table(c)) == NULL) {
				// Não foi possível encontrar a entrada desse caractere na tabela.
				return 1;
			}
		} else {
			entry = NULL;
		}

		entries[entries_found++] = entry;
	}

	return leds_drv_blink_morse(led, entries_found, entries, end_callback);
}

static uint8_t is_lower_alpha(char c)
{
	return c >= 'a' && c <= 'z';
}

static uint8_t is_upper_alpha(char c)
{
	return c >= 'A' && c <= 'Z';
}

static uint8_t is_numeric(char c)
{
	return c >= '0' && c <= '9';
}

static uint8_t is_symbol(char c)
{
	return c == '&' || c == '\'' || c == '@' || c == ')' || c == '(' ||
	       c == ':' || c == ',' || c == '=' || c == '.' || c == '-' ||
	       c == '%' || c == '+' || c == '\"' || c == '?' || c == '/' ||
	       c == '!';
}

static const struct morse_entry *get_entry_from_table(char key)
{
	for (uint8_t u = 0; morse_table[u].entry != NULL; ++u) {
		if (morse_table[u].key == key) {
			return &morse_table[u];
		}
	}

	return NULL;
}
