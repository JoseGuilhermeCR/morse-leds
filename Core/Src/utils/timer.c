/*
 * Timer.c
 *
 *  Created on: Jun 4, 2021
 *      Author: joseg
 */

#include "stm32f7xx_hal.h"

#include "utils/timer.h"

#define MAX_TIME ((timer_t)-1)

#define relactiveTime(init_t, final_t)                                         \
	(init_t > final_t) ? (((MAX_TIME - init_t) + final_t) + 1) :           \
				   (final_t - init_t)

void timer_reset(timer_t *timer)
{
	*timer = HAL_GetTick();
}

uint32_t timer_get_elapsed_time(timer_t timer)
{
	timer_t ft = HAL_GetTick();
	return (uint32_t)relactiveTime(timer, ft);
}

uint8_t timer_wait_ms(timer_t timer, uint32_t time_ms)
{
	timer_t ft = HAL_GetTick();
	return !(relactiveTime(timer, ft) < time_ms);
}

uint8_t timer_wait_sec(timer_t timer, uint16_t time_sec)
{
	timer_t ft = HAL_GetTick();
	return !(relactiveTime(timer, ft) < ((timer_t)time_sec * 1000));
}
