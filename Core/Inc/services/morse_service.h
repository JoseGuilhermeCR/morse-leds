/*
 * morse_service.h
 *
 *  Created on: Jun 5, 2021
 *      Author: joseg
 */

#ifndef MORSE_SERVICE_H_
#define MORSE_SERVICE_H_

#include <stdint.h>

#include "drivers/leds_driver.h"

struct morse_entry {
	char key;

	uint8_t entry_size;
	const char *entry;
};

/**
 * @brief Inicializa serviço de código morse.
 * 
 * @return uint8_t 0 em sucesso, 1 em falha.
 */
uint8_t morse_srv_init();

/**
 * @brief Executa o serviço para que o código seja mostrado.
 * 
 * @return uint8_t 0 em sucesso, 1 em falha.
 */
uint8_t morse_srv_handler();

/**
 * @brief Desinicializa o serviço de código morse.
 * 
 * @return uint8_t 
 */
uint8_t morse_srv_deinit();

/**
 * @brief Pede o serviço para piscar uma mensagem em código morse.
 * 
 * @param led O Led que deverá ser usado para piscar a mensagem.
 * @param message A mensagem que será traduzida para morse e depois mostrada.
 * @param end_callback Um callback opcional que será chamado ao término da exibição.
 * @return uint8_t 
 */
uint8_t morse_srv_blink(enum leds led, const char *message,
			void (*end_callback)());

#endif /* MORSE_SERVICE_H_ */
