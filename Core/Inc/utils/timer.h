/*
 * Timer.h
 *
 *  Created on: Jun 4, 2021
 *      Author: joseg
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>

typedef uint32_t timer_t;

/**
 * @brief Reseta o timer.
 *
 * @param timer Ponteiro para timer que será reiniciado.
 * @return RETURN_STATUS
 */
void timer_reset(timer_t *timer);

/**
 * @brief Checa o tempo decorrido desde quando o timer foi reiniciado.
 *
 * @param timer Timer a ser checado.
 * @return uint32_t Tempo decorrido.
 */
uint32_t timer_get_elapsed_time(timer_t timer);

/**
 * @brief Indica quando o tempo esperado expirou em milisegundos.
 *
 * @param timer Timer a ser checado.
 * @param time_ms Valor de tempo em milissegundos.
 * @return uint8_t 0 caso tempo  expirado, 1 caso tempo expirado.
 */
uint8_t timer_wait_ms(timer_t timer, uint32_t time_ms);

/**
 * @brief Indica quando o tempo esperado expirou em segundos.
 *
 * @param timer Timer a ser checado.
 * @param time_sec Valor de tempo em segundos.
 * @return uint8_t 0 caso tempo  expirado, 1 caso tempo expirado.
 */
uint8_t timer_wait_sec(timer_t timer, uint16_t time_sec);

#endif /* TIMER_H_ */
