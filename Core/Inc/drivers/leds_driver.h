/*
 * leds_driver.h
 *
 *  Created on: 5 de jun de 2021
 *      Author: joseg
 */

#ifndef LEDS_DRIVER_H
#define LEDS_DRIVER_H

#include <stdint.h>

#include "drivers/leds_driver.h"

// Forward Declaration.
struct morse_entry;

enum leds {
	LEDS_RED = 0,
	LEDS_GREEN,
	LEDS_BLUE,

	LEDS_SIZE
};

/**
 * @brief Inicializa o driver de leds.
 * 
 * @return uint8_t 0 em sucesso, 1 em falha.
 */
uint8_t leds_drv_init();

/**
 * @brief Executa o driver de leds para que os leds sejam controlados.
 * 
 * @return uint8_t 0 em sucesso, 1 em falha.
 */
uint8_t leds_drv_handler();

/**
 * @brief Desinicializa o driver de leds.
 * 
 * @return uint8_t 0 em sucesso, 1 em falha.
 */
uint8_t leds_drv_deinit();

/**
 * @brief Começa a exibição do código morse em um led.
 * 
 * @param led O Led que deverá ser usado para piscar a mensagem.
 * @param size Tamanho do arranjo de entradas da tabela de morse.
 * @param entries Arranjo com ponteiros para entradas na tabela de morse.
 * Esse arranjo será salvo em um buffer interno, portanto não é preciso
 * se preocupar em como ele é guardado externamente. Só é preciso garantir
 * que seja válido.
 * @param blink_end_callback Um callback opcional que será chamado ao término da exibição.
 * @return uint8_t 0 em sucesso, 1 em falha.
 */
uint8_t leds_drv_blink_morse(enum leds led, uint8_t size,
			     const struct morse_entry **entries,
			     void (*blink_end_callback)());

#endif /* LEDS_DRIVER_H */
